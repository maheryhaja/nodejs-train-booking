import { ExecutionContext, INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as mongoose from 'mongoose';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { JwtAuthGuard } from '../src/auth/jwt-auth.guard';
import { Client } from '../src/client/entities/client.entity';
import { Ticket, TicketStatus } from '../src/ticket/entities/ticket.entity';
import { TrainRide } from '../src/train-ride/entities/train-ride.entity';

describe('AppController (e2e)', () => {
    let app: INestApplication;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(JwtAuthGuard)
            .useValue({
                canActivate: (context: ExecutionContext) => {
                    const req = context.switchToHttp().getRequest();
                    req.user = {
                        client: {
                            _id: new mongoose.Types.ObjectId('6420a99a957047fccf062296'),
                            address: 'some address',
                            createdAt: new Date(),
                            email: 'client-test@yopmail.com',
                            firstName: 'client',
                            lastName: 'test',
                            isActive: true,
                            phones: [],
                            reference: 'cl00',
                            user: undefined,
                        } as Client,
                    };
                    return true;
                },
            })
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('Should be able to book ticket and cancel ticket', async () => {
        // get available train rides
        const availableTrainRidesResponse = await request(app.getHttpServer()).get('/train-ride?page=1&pageSize=15');
        expect(availableTrainRidesResponse.status).toBe(200);
        const trainRides: TrainRide[] = availableTrainRidesResponse.body.items;

        expect(trainRides.length).toBeGreaterThan(0);

        // get available tickets for specified train ride
        const availableTicketsResponse = await request(app.getHttpServer()).get(
            `/ticket?page=1&pageSize=15&trainRideId=${trainRides[0]._id}`
        );
        expect(availableTicketsResponse.status).toBe(200);
        const tickets: Ticket[] = availableTicketsResponse.body.items;
        const ticket = tickets[0];
        expect(tickets.length).toBeGreaterThan(0);

        // book ticket
        const bookTicketsResponse = await request(app.getHttpServer())
            .post('/booking/book')
            .send({ ticketIds: [ticket._id] });
        expect(bookTicketsResponse.status).toBe(201);
        const bookingId: string = bookTicketsResponse.body._id;

        // check if ticket is booked
        let ticketResponse = await request(app.getHttpServer()).get(`/ticket/${ticket._id}`);
        expect(ticketResponse.status).toBe(200);
        expect(ticketResponse.body.status).toBe(TicketStatus.BOOKED);

        // cancel booking
        const cancelBookingResponse = await request(app.getHttpServer()).put(`/booking/cancel/${bookingId}`);
        expect(cancelBookingResponse.status).toBe(200);

        // check if ticket is available
        ticketResponse = await request(app.getHttpServer()).get(`/ticket/${ticket._id}`);
        expect(ticketResponse.status).toBe(200);
        expect(ticketResponse.body.status).toBe(TicketStatus.AVAILABLE);

        return true;
    });
});
