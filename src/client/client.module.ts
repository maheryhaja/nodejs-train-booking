import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CounterModule } from '../counter/counter.module';
import { MailNotificationModule } from '../mail-notification/mail-notification.module';
import { MailTokenModule } from '../mail-token/mail-token.module';
import { MailModule } from '../mail/mail.module';
import { UsersModule } from '../users/users.module';
import { ClientController } from './client.controller';
import { ClientRepository } from './client.repository';
import { ClientService } from './client.service';
import { Client, ClientSchema } from './entities/client.entity';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Client.name, schema: ClientSchema }]),
        CounterModule,
        UsersModule,
        MailTokenModule,
        MailNotificationModule,
        MailModule,
    ],
    controllers: [ClientController],
    providers: [ClientService, ClientRepository],
    exports: [ClientService],
})
export class ClientModule {}
