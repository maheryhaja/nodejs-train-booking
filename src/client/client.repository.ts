import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { Client, ClientDocument } from './entities/client.entity';

@Injectable()
export class ClientRepository extends BaseRepository<ClientDocument, Client> {
    constructor(@InjectModel(Client.name) clientModel) {
        super(clientModel);
    }
}
