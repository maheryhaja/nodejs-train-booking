import { Client } from '../entities/client.entity';

export type CreateClientDto = Pick<Client, 'firstName' | 'lastName' | 'email' | 'phones' | 'address'> & { password: string };
