import { Client } from '../entities/client.entity';

export type UpdateClientDto = Partial<Client>;
