import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { HydratedDocument } from 'mongoose';
import { User } from '../../users/users.entity';

@Schema()
export class Client {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _id: any;

    @Prop({ type: String, required: true })
    firstName: string;

    @Prop({ type: String, required: true })
    lastName: string;

    @Prop({ type: String, required: true })
    email: string;

    @Prop({ type: [String] })
    phones: string[];

    @Prop({ type: String, required: true })
    address: string;

    @Prop({ type: String, required: true })
    reference: string;

    @Prop({ type: Date, required: true })
    createdAt: Date;

    @Prop({ type: Date })
    updatedAt?: Date;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true })
    user: User;

    @Prop({ type: Boolean, required: true, default: false })
    isActive: boolean;
}

export type ClientDocument = HydratedDocument<Client>;

export const ClientSchema = SchemaFactory.createForClass(Client);
