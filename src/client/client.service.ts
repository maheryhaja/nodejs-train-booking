import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CounterService } from '../counter/counter.service';
import { MailNotificationService } from '../mail-notification/mail-notification.service';
import { MailTokenType } from '../mail-token/entities/mail-token.entity';
import { MailTokenService } from '../mail-token/mail-token.service';
import { MailRenderService } from '../mail/mail-render.service';
import { User, UserRole } from '../users/users.entity';
import { UsersService } from '../users/users.service';
import { ClientRepository } from './client.repository';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import { Client } from './entities/client.entity';

@Injectable()
export class ClientService {
    // eslint-disable-next-line max-params
    constructor(
        private readonly clientRepository: ClientRepository,
        private readonly counterService: CounterService,
        private readonly userService: UsersService,
        private readonly mailTokenService: MailTokenService,
        private readonly mailNotificationService: MailNotificationService,
        private readonly mailRenderService: MailRenderService
    ) {}

    private throwEmailAlreadyExists() {
        throw new HttpException('Email already used', HttpStatus.CONFLICT);
    }

    async emailAlreadyExists(email: string): Promise<boolean> {
        const clients = await this.clientRepository.find({ email }).exec();
        return clients.length > 0;
    }

    async create(createClientDto: CreateClientDto, user: User): Promise<Client> {
        return this.clientRepository.create({
            ...createClientDto,
            createdAt: new Date(),
            reference: await this.counterService.requestReference('CL'),
            user,
            isActive: false,
        });
    }

    async signUp(createClientDto: CreateClientDto): Promise<Client> {
        if (await this.emailAlreadyExists(createClientDto.email)) {
            this.throwEmailAlreadyExists();
        }
        const user = await this.userService.create({
            password: createClientDto.password,
            role: UserRole.CLIENT,
            username: createClientDto.email,
        });
        const client = await this.create(createClientDto, user);
        const mailToken = await this.mailTokenService.requestToken(MailTokenType.CLIENT_ACTIVATION, client._id);
        await this.mailNotificationService.create({
            content: await this.mailRenderService.renderActivationAccount(client, mailToken.token),
            to: client.email,
            subject: 'Activation de compte',
        });
        return client;
    }

    async activate(activationToken: string): Promise<Client> {
        const mailToken = await this.mailTokenService.findByToken(activationToken);
        if (!mailToken) {
            throw new HttpException('Invalid token', HttpStatus.FORBIDDEN);
        }
        const client = await this.findOne(mailToken.entityId);
        if (!client) {
            throw new HttpException('Client not found', HttpStatus.NOT_FOUND);
        }
        await this.update(client._id, {
            isActive: true,
        });
        return this.findOne(client._id);
    }

    async activateAccount(clientId: string) {
        await this.update(clientId, {
            isActive: true,
        });
    }

    async isClientUserActive(userId: string) {
        return !!(await this.clientRepository.findOne({ user: userId, isActive: true }).exec());
    }

    findAll() {
        return this.clientRepository.find({});
    }

    findOne(id: string) {
        return this.clientRepository.findById(id).lean().exec();
    }

    update(id: string, updateClientDto: UpdateClientDto) {
        return this.clientRepository.update(id, updateClientDto);
    }

    remove(id: string) {
        return this.clientRepository.delete(id);
    }

    async findClientByUserId(userId: string): Promise<Client | undefined> {
        return this.clientRepository.findOne({ user: userId }).exec();
    }
}
