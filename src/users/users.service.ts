import { Injectable } from '@nestjs/common';
import { User, UserRole } from './users.entity';
import { UserRepository } from './users.repository';

@Injectable()
export class UsersService {
    private readonly users: User[] = [
        {
            _id: '1',
            username: 'john',
            password: 'changeme',
            role: UserRole.ADMIN,
        },
        {
            _id: '2',
            username: 'maria',
            password: 'guess',
            role: UserRole.CLIENT,
        },
    ];

    constructor(private readonly userRepository: UserRepository) {}

    async findOne(username: string): Promise<User | undefined> {
        return this.userRepository.findOne({ username }).exec();
    }

    async findById(id: string): Promise<User | undefined> {
        return this.userRepository.findById(id);
    }

    async create(user: Omit<User, '_id'>): Promise<User> {
        return this.userRepository.create(user);
    }
}
