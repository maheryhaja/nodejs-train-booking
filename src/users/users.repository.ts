import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { User, UserDocument } from './users.entity';

@Injectable()
export class UserRepository extends BaseRepository<UserDocument, User> {
    constructor(@InjectModel(User.name) userModel: Model<UserDocument>) {
        super(userModel);
    }
}
