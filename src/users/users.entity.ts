import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { HydratedDocument } from 'mongoose';
import { Client } from '../client/entities/client.entity';

export type UserDocument = HydratedDocument<User>;

export enum UserRole {
    ADMIN = 'ADMIN',
    CLIENT = 'CLIENT',
}

@Schema()
export class User {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _id: any;

    @Prop({ type: String, required: true })
    username: string;

    @Prop({ type: String, required: true })
    password: string;

    @Prop({ type: String, enum: Object.values(UserRole), required: true })
    role: UserRole;
    client?: Client;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.pre<UserDocument>('save', async function (next) {
    if (this.isModified('password') || this.isNew) {
        this.password = await bcrypt.hash(this.password, 10);
    }

    next();
});
