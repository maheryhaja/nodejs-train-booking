import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import * as fs from 'fs';
import * as path from 'path';
import { database } from './app.database';
import { AppModule } from './app.module';
import { DbScriptService } from './db-script/db-script.service';
import { MailNotificationService } from './mail-notification/mail-notification.service';
export type ScriptFn = () => Promise<void>;

const SCRIPT_DIRECTORY = 'db-script/scripts';

const sleep = async (millis: number) => new Promise((resolve) => setTimeout(() => resolve(1), millis));

const mainLoop = async (mailNotificationService: MailNotificationService) => {
    while (true) {
        await mailNotificationService.sendAllPendingMail();
        await sleep(10000);
    }
};

export const launchDbScripts = async (dbScriptService: DbScriptService) => {
    const dbScriptFilenames = fs
        .readdirSync(fs.existsSync('db-script') ? SCRIPT_DIRECTORY : `src/${SCRIPT_DIRECTORY}`)
        .filter((name) => !(name.endsWith('d.ts') || name.endsWith('d.js') || name.endsWith('map')));
    dbScriptFilenames.sort();
    for (const filename of dbScriptFilenames) {
        const filepath = path.join(__dirname, `../src/db-script/scripts/${path.parse(filename).name}`);
        const { scripts } = await import(filepath);
        for (const script of scripts as ScriptFn[]) {
            const dbScript = { filename, script: script.name };
            const alreadyExecuted = await dbScriptService.findOne(dbScript);
            if (!alreadyExecuted) {
                // eslint-disable-next-line no-console
                console.log(`Executing database script named ${script.name} from file ${filename}`);
                await script();
                await dbScriptService.create(dbScript);
            }
        }
    }
};

async function bootstrap() {
    database.connect(async () => {
        const app = await NestFactory.create(AppModule);
        const configService: ConfigService = app.get(ConfigService);
        const dbScriptService: DbScriptService = app.get(DbScriptService);
        const mailNotificationService: MailNotificationService = app.get(MailNotificationService);
        await launchDbScripts(dbScriptService);
        const port = configService.get<number>('server.port');
        await app.listen(port);
        mainLoop(mailNotificationService);
    });
}
bootstrap();
