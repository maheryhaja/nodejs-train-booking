import { Injectable } from '@nestjs/common';
import { CounterService } from '../counter/counter.service';
import { CreateTrainSeatDto } from './dto/create-train-seat.dto';
import { UpdateTrainSeatDto } from './dto/update-train-seat.dto';
import { TrainSeatRepository } from './train-seat.repository';

@Injectable()
export class TrainSeatService {
    constructor(private readonly trainSeatRepository: TrainSeatRepository, private readonly counterService: CounterService) {}

    async create(createTrainSeatDto: CreateTrainSeatDto) {
        return this.create({
            ...createTrainSeatDto,
            reference: await this.counterService.requestReference('ST'),
        });
    }

    findAll() {
        return this.trainSeatRepository.find({});
    }

    findOne(id: string) {
        return this.trainSeatRepository.findOne({ _id: id });
    }

    update(id: string, updateTrainSeatDto: UpdateTrainSeatDto) {
        return this.trainSeatRepository.update(id, updateTrainSeatDto);
    }

    remove(id: string) {
        return this.trainSeatRepository.delete(id);
    }
}
