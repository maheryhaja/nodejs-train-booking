import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CounterModule } from '../counter/counter.module';
import { TrainSeat, TrainSeatSchema } from './entities/train-seat.entity';
import { TrainSeatController } from './train-seat.controller';
import { TrainSeatRepository } from './train-seat.repository';
import { TrainSeatService } from './train-seat.service';

@Module({
    imports: [MongooseModule.forFeature([{ name: TrainSeat.name, schema: TrainSeatSchema }]), CounterModule],
    controllers: [TrainSeatController],
    providers: [TrainSeatService, TrainSeatRepository],
})
export class TrainSeatModule {}
