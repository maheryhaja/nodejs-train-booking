import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Train } from '../../train/entities/train.entity';

export enum TrainSeatType {
    STANDARD = 'STANDARD',
    VIP = 'VIP',
}

@Schema()
export class TrainSeat {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _id: any;

    @Prop({ type: String, required: true })
    reference: string;

    @Prop({ type: String, enum: Object.values(TrainSeatType), required: true })
    type: TrainSeatType;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Train', required: true })
    train: Train;
}

export type TrainSeatDocument = mongoose.HydratedDocument<TrainSeat>;

export const TrainSeatSchema = SchemaFactory.createForClass(TrainSeat);
