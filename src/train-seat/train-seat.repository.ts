import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { Train } from '../train/entities/train.entity';
import { TrainSeat, TrainSeatDocument } from './entities/train-seat.entity';

export class TrainSeatRepository extends BaseRepository<TrainSeatDocument, Train> {
    constructor(@InjectModel(TrainSeat.name) trainSeatModel) {
        super(trainSeatModel);
    }
}
