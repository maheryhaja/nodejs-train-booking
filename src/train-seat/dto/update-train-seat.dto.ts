import { TrainSeat } from '../entities/train-seat.entity';

export type UpdateTrainSeatDto = Partial<TrainSeat>;
