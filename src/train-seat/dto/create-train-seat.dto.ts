import { TrainSeat } from '../entities/train-seat.entity';

export type CreateTrainSeatDto = Omit<TrainSeat, '_id'>;
