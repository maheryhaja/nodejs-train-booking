import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { UserRole } from '../users/users.entity';
import { CreateTrainSeatDto } from './dto/create-train-seat.dto';
import { UpdateTrainSeatDto } from './dto/update-train-seat.dto';
import { TrainSeatService } from './train-seat.service';

@Controller('train-seat')
export class TrainSeatController {
    constructor(private readonly trainSeatService: TrainSeatService) {}

    @Post()
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(UserRole.ADMIN)
    create(@Body() createTrainSeatDto: CreateTrainSeatDto) {
        return this.trainSeatService.create(createTrainSeatDto);
    }

    @Get()
    findAll() {
        return this.trainSeatService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.trainSeatService.findOne(id);
    }

    @Patch(':id')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(UserRole.ADMIN)
    update(@Param('id') id: string, @Body() updateTrainSeatDto: UpdateTrainSeatDto) {
        return this.trainSeatService.update(id, updateTrainSeatDto);
    }

    @Delete(':id')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(UserRole.ADMIN)
    remove(@Param('id') id: string) {
        return this.trainSeatService.remove(id);
    }
}
