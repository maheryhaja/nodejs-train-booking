import { Injectable } from '@nestjs/common';
import { DbScriptRepository } from './db-script.repository';
import { DbScript } from './db-script.schema';

@Injectable()
export class DbScriptService {
    constructor(private readonly dbScriptRepository: DbScriptRepository) {}

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async findOne(criteria: any): Promise<DbScript | null> {
        return this.dbScriptRepository.findOne(criteria).exec();
    }
    async create(item: DbScript): Promise<DbScript> {
        return this.dbScriptRepository.create(item);
    }
}
