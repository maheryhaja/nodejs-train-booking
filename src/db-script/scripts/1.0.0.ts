import * as bcrypt from 'bcrypt';
import { DateTime } from 'luxon';
import { ScriptFn } from '../../main';
import { CreateTicketDto } from '../../ticket/dto/create-ticket.dto';
import { TicketStatus } from '../../ticket/entities/ticket.entity';
import { CreateTrainRideDto } from '../../train-ride/dto/create-train-ride.dto';
import { TrainRide } from '../../train-ride/entities/train-ride.entity';
import { CreateTrainSeatDto } from '../../train-seat/dto/create-train-seat.dto';
import { TrainSeat, TrainSeatType } from '../../train-seat/entities/train-seat.entity';
import { CreateTrainDto } from '../../train/dto/create-train.dto';
import { UserRole } from '../../users/users.entity';

import { bulkCreate, create, findMany } from '../db-script.utils';

const addSuperAdminUser: ScriptFn = async () => {
    await create('users', {
        username: 'admin',
        password: await bcrypt.hash('admin', 10),
        role: UserRole.ADMIN,
    });
};

const addTrainMock: ScriptFn = async () => {
    const trains: CreateTrainDto[] = [
        {
            name: 'Lyon Express',
            trainModel: 'Locomotive Express',
        },
        {
            name: 'Marseille Express',
            trainModel: 'TGV',
        },
    ];

    for (const train of trains) {
        await create('trains', train);
    }
};

const addSeatsMock: ScriptFn = async () => {
    const trains = await findMany('trains', {});

    const seats: CreateTrainSeatDto[] = trains.flatMap((train, i) =>
        Array.from(
            new Array(20),
            (_, j): CreateTrainSeatDto => ({
                reference: `${i === 0 ? 'LEX' : 'TG'}_${j.toString().padStart(4, '0')}`,
                train: train._id,
                type: j < 20 ? TrainSeatType.STANDARD : TrainSeatType.VIP,
            })
        )
    );

    await bulkCreate('trainseats', seats);
};

const addTrainRides: ScriptFn = async () => {
    const trains = await findMany('trains', {});
    const trainRides: CreateTrainRideDto[] = trains.flatMap((train, i) =>
        Array.from(
            new Array(30),
            (_, j): CreateTrainRideDto => ({
                departure: DateTime.fromISO('2023-03-27T08:00:00').plus({ day: j }).toJSDate(),
                arrival: DateTime.fromISO('2023-03-27T11:00:00').plus({ day: j }).toJSDate(),
                reference: `${i === 0 ? 'RLEX' : 'RTG'}_${j.toString().padStart(4, '0')}`,
                train: train._id,
                from: 'Paris',
                to: `${i === 0 ? 'Lyon' : 'Marseille'}`,
            })
        )
    );

    await bulkCreate('trainrides', trainRides);
};

const addTickets: ScriptFn = async () => {
    const trainRides: TrainRide[] = await findMany('trainrides', {});
    let count = 0;
    const tickets: CreateTicketDto[] = [];
    for (const trainRide of trainRides) {
        const seats: TrainSeat[] = await findMany('trainseats', { train: trainRide.train });
        for (const seat of seats) {
            tickets.push({
                price: seat.type === TrainSeatType.VIP ? 40 : 20,
                reference: `TK_${count.toString().padStart(6, '0')}`,
                seat: seat._id,
                status: TicketStatus.AVAILABLE,
                trainRide: trainRide._id,
            });
            count++;
        }
    }
    await bulkCreate('tickets', tickets);
};

export const scripts: ScriptFn[] = [addSuperAdminUser, addTrainMock, addSeatsMock, addTrainRides, addTickets];
