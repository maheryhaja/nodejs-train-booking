import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { DbScript, DbScriptDocument } from './db-script.schema';

@Injectable()
export class DbScriptRepository extends BaseRepository<DbScriptDocument, DbScript> {
    constructor(@InjectModel(DbScript.name) userModel: Model<DbScriptDocument>) {
        super(userModel);
    }
}
