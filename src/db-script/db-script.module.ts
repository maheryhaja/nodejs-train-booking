import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DbScriptRepository } from './db-script.repository';
import { DbScript, DbScriptSchema } from './db-script.schema';
import { DbScriptService } from './db-script.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: DbScript.name,
                schema: DbScriptSchema,
            },
        ]),
    ],
    providers: [DbScriptService, DbScriptRepository],
    exports: [DbScriptService],
})
export class DbScriptModule {}
