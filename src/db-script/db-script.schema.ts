import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type DbScriptDocument = HydratedDocument<DbScript>;

@Schema()
export class DbScript {
    @Prop({ type: String, required: true, trim: true })
    filename: string;

    @Prop({ type: String, required: true, trim: true })
    script: string;

    @Prop({ type: Date, required: true, default: new Date() })
    executedAt?: Date;
}

export const DbScriptSchema = SchemaFactory.createForClass(DbScript);
