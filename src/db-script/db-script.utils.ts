import * as mongoose from 'mongoose';

export type CreatedDocument<T> = T & { _id: string };

type CollectionName = 'users' | 'mailnotifications' | 'trains' | 'trainseats' | 'trainrides' | 'tickets';

export const dbCollection = (collection: CollectionName) => mongoose.connection.db.collection(collection);

export const dropDatabase = async () => {
    await Promise.all(mongoose.modelNames().map((model) => mongoose.model(model).deleteMany({})));
};

export const create = <T>(collection: CollectionName, document: T) =>
    dbCollection(collection)
        .insertOne(document)
        .then((result) => result.insertedId);

export const bulkCreate = <T>(collection: CollectionName, documents: T[]) => dbCollection(collection).insertMany(documents);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const findOneAndUpdate = <T>(collection: CollectionName, filter: any, update: any): Promise<CreatedDocument<T>> =>
    dbCollection(collection)
        .findOneAndUpdate(filter, update)
        .then((result) => result.value as unknown as CreatedDocument<T>);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const findMany = <T>(collection: CollectionName, filter: any): Promise<any[]> =>
    dbCollection(collection).find(filter).toArray();

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const findOne = <T>(collection: CollectionName, filter: any) =>
    dbCollection(collection)
        .findOne(filter, {})
        .then((result) => result);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const deleteMany = <T>(collection: CollectionName, filter: any): Promise<null> =>
    dbCollection(collection)
        .deleteMany(filter)
        .then(() => null);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const rename = (collection: CollectionName, filter: any, newName: any): Promise<null> =>
    dbCollection(collection)
        .updateOne(filter, { $rename: { ...newName } })
        .then(() => null);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const updateMany = <T>(collection: CollectionName, filter: any, update: any): Promise<null> =>
    dbCollection(collection)
        .updateMany(filter, { ...update })
        .then(() => null);
