/* eslint-disable no-console */
import * as mongoose from 'mongoose';
import configuration from '../config/configuration';

class Database {
    connect(cb: () => void) {
        const connexionString = configuration().mongodb.dbURI;

        mongoose.connect(connexionString);

        mongoose.connection.on('connected', () => {
            console.log(`connected to database: ${connexionString}`);
        });

        mongoose.connection.once('open', () => {
            console.log(`First connection to database: ${connexionString}`);
            cb();
        });

        mongoose.connection.on('error', () => {
            throw new Error(`unable to connect to database: ${connexionString}`);
        });

        mongoose.connection.on('disconnected', () => {
            console.log(`Disconnected to database: ${connexionString}`);
        });
    }
}

export const database = new Database();
