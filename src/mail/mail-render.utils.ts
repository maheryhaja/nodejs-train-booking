import { DateTime } from 'luxon';

export module MailRenderUtils {
    export const formatDateTime = (date: Date) => DateTime.fromJSDate(date).toFormat('dd/MM/yyyy HH:mm');
}
