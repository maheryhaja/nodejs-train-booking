import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as ejs from 'ejs';
import { Booking } from '../booking/entities/booking.entity';
import { Client } from '../client/entities/client.entity';
import { MailRenderUtils } from './mail-render.utils';

const HELLO_MAIL_TEMPLATE = './public/email-template/hello.ejs';
const BOOKING_CONFIRMATION_TEMPLATE = './public/email-template/booking-confirmation.ejs';
const BOOKING_CANCELATION_TEMPLATE = './public/email-template/booking-cancelation.ejs';
const ACCOUNT_ACTIVATION_TEMPLATE = './public/email-template/account-activation.ejs';

@Injectable()
export class MailRenderService {
    constructor(private readonly configService: ConfigService) {}

    async renderHello() {
        return ejs.renderFile(HELLO_MAIL_TEMPLATE, {});
    }

    async renderBookingConfirmation(client: Client, booking: Booking) {
        return ejs.renderFile(BOOKING_CONFIRMATION_TEMPLATE, {
            client,
            booking,
            formatDate: MailRenderUtils.formatDateTime,
        });
    }

    async renderBookingCancelation(client: Client, booking: Booking) {
        return ejs.renderFile(BOOKING_CANCELATION_TEMPLATE, {
            client,
            booking,
            formatDate: MailRenderUtils.formatDateTime,
        });
    }

    async renderActivationAccount(client: Client, token: string) {
        return ejs.renderFile(ACCOUNT_ACTIVATION_TEMPLATE, {
            client,
            token,
            baseUrl: this.configService.get('mail.apiBaseUrl'),
        });
    }
}
