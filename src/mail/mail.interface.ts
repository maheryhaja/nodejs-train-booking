import { NotificationAttachment } from '../mail-notification/entities/mail-notification.entity';

export interface MailPayload {
    from?: string;
    to: string;
    subject: string;
    content: string;
    attachments?: NotificationAttachment[];
}
