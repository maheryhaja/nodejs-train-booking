import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as fs from 'fs';
import * as nodemailer from 'nodemailer';
import { MailPayload } from './mail.interface';

@Injectable()
export class MailService {
    constructor(private readonly configService: ConfigService) {}
    async sendMail(payload: MailPayload) {
        await this.getTransporter().sendMail({
            from: payload?.from || this.configService.get('smtp.user'),
            to: payload.to,
            subject: payload.subject,
            html: payload.content,
            ...(!payload.attachments
                ? {}
                : {
                      attachments: payload.attachments.map((it) => ({
                          filename: it.filename,
                          content: fs.createReadStream(`public/${it.path}`),
                      })),
                  }),
        });
    }

    getTransporter() {
        return nodemailer.createTransport({
            host: this.configService.get('smtp.host'),
            port: this.configService.get('smtp.port'),
            auth: {
                user: this.configService.get('smtp.user'),
                pass: this.configService.get('smtp.password'),
            },
        });
    }
}
