import { forwardRef, Module } from '@nestjs/common';
import { AppModule } from '../app.module';
import { MailRenderService } from './mail-render.service';
import { MailService } from './mail.service';

@Module({
    imports: [forwardRef(() => AppModule)],
    providers: [MailService, MailRenderService],
    exports: [MailService, MailRenderService],
})
export class MailModule {}
