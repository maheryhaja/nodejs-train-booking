import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ClientService } from '../client/client.service';
import { Client } from '../client/entities/client.entity';
import { CounterService } from '../counter/counter.service';
import { MailNotificationService } from '../mail-notification/mail-notification.service';
import { MailRenderService } from '../mail/mail-render.service';
import { TicketService } from '../ticket/ticket.service';
import { BookingRepository } from './booking.repository';
import { BookTicketsParamDto } from './dto/book-tickets-param.dto';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';
import { Booking, BookingStatus } from './entities/booking.entity';

@Injectable()
export class BookingService {
    // eslint-disable-next-line max-params
    constructor(
        private readonly bookingRepository: BookingRepository,
        private readonly counterService: CounterService,
        private readonly clientService: ClientService,
        private readonly ticketService: TicketService,
        private readonly mailNotificationService: MailNotificationService,
        private readonly mailRenderService: MailRenderService
    ) {}

    async create(createBookingDto: CreateBookingDto) {
        const created = await this.bookingRepository.create({
            ...createBookingDto,
            status: BookingStatus.VALIDATED,
            createdAt: new Date(),
            reference: await this.counterService.requestReference('BOOK'),
        });
        return this.findOne(created._id);
    }

    findAll() {
        return this.bookingRepository.find({});
    }

    async findOneBasedOnUser(bookingId: string, client: Client) {
        const booking = await this.findOne(bookingId);
        if (!booking) {
            throw new HttpException('Booking not found', HttpStatus.NOT_FOUND);
        }

        if (`${client._id}` !== `${booking.client._id}`) {
            throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
        }
        return booking;
    }

    async myBookings(client: Client) {
        return this.bookingRepository.find({ client }).populate('client tickets').exec();
    }

    findOne(id: string) {
        return this.bookingRepository.findById(id).populate('tickets client').lean().exec();
    }

    update(id: string, updateBookingDto: UpdateBookingDto) {
        return this.bookingRepository.update(id, updateBookingDto);
    }

    remove(id: string) {
        return this.bookingRepository.delete(id);
    }

    async bookTickets(param: BookTicketsParamDto): Promise<Booking> {
        const client = param.client;
        if (!(await this.ticketService.isAllTicketsAvailable(param.ticketIds))) {
            throw new HttpException('Some of tickets are not available', HttpStatus.FORBIDDEN);
        }
        const tickets = await this.ticketService.getTickets(param.ticketIds);

        const booking = await this.create({
            client,
            tickets,
        });
        await this.ticketService.markAsBooked(booking.tickets as string[], client);
        await this.mailNotificationService.create({
            content: await this.mailRenderService.renderBookingConfirmation(client, { ...booking, tickets }),
            subject: 'Confirmation de Réservation',
            to: client.email,
        });

        return this.findOne(booking._id);
    }

    async cancel(bookingId: string, client: Client): Promise<Booking> {
        const booking = await this.findOne(bookingId);
        if (!booking) {
            throw new HttpException('Booking not found', HttpStatus.NOT_FOUND);
        }
        await this.update(bookingId, {
            canceledAt: new Date(),
            status: BookingStatus.CANCELED,
        });
        await this.ticketService.cancel(booking.tickets.map((it) => it._id));
        await this.mailNotificationService.create({
            content: await this.mailRenderService.renderBookingCancelation(client, {
                ...booking,
                tickets: await this.ticketService.getTickets(booking.tickets.map((it) => it._id)),
            }),
            subject: 'Annulation de réservation',
            to: client.email,
        });
        return this.findOne(booking._id);
    }
}
