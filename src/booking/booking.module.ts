import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ClientModule } from '../client/client.module';
import { CounterModule } from '../counter/counter.module';
import { MailNotificationModule } from '../mail-notification/mail-notification.module';
import { MailTokenModule } from '../mail-token/mail-token.module';
import { MailModule } from '../mail/mail.module';
import { TicketModule } from '../ticket/ticket.module';
import { BookingController } from './booking.controller';
import { BookingRepository } from './booking.repository';
import { BookingService } from './booking.service';
import { Booking, BoookingSchema } from './entities/booking.entity';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Booking.name, schema: BoookingSchema }]),
        CounterModule,
        ClientModule,
        TicketModule,
        MailNotificationModule,
        MailTokenModule,
        MailModule,
    ],
    controllers: [BookingController],
    providers: [BookingService, BookingRepository],
})
export class BookingModule {}
