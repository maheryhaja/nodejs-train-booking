import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { Booking, BookingDocument } from './entities/booking.entity';

@Injectable()
export class BookingRepository extends BaseRepository<BookingDocument, Booking> {
    constructor(@InjectModel(Booking.name) bookingModel) {
        super(bookingModel);
    }
}
