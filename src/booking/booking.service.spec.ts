import { ExecutionContext } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../app.module';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { TicketModule } from '../ticket/ticket.module';
import { TicketService } from '../ticket/ticket.service';
import { BookingService } from './booking.service';

describe('BookingService', () => {
    let service: BookingService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [TicketModule, AppModule],
            providers: [BookingService, TicketService],
        })
            .overrideGuard(JwtAuthGuard)
            .useValue({
                canActivate: (context: ExecutionContext) => {
                    const req = context.switchToHttp().getRequest();
                },
            })
            .compile();

        service = module.get<BookingService>(BookingService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
