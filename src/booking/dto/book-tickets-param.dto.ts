import { Client } from '../../client/entities/client.entity';

export interface BookTicketsParamDto {
    ticketIds: string[];
    client: Client;
}
