import { Booking } from '../entities/booking.entity';

export type UpdateBookingDto = Partial<Booking>;
