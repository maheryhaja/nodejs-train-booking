import { Booking } from '../entities/booking.entity';

export type CreateBookingDto = Omit<Booking, '_id' | 'createdAt' | 'reference' | 'status'>;
