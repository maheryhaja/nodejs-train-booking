import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Request, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { UserRole } from '../users/users.entity';
import { BookingService } from './booking.service';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';

@Controller('booking')
export class BookingController {
    constructor(private readonly bookingService: BookingService) {}

    @Post()
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(UserRole.ADMIN)
    create(@Body() createBookingDto: CreateBookingDto) {
        return this.bookingService.create(createBookingDto);
    }

    @Get()
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(UserRole.ADMIN)
    findAll() {
        return this.bookingService.findAll();
    }

    @Post('book')
    @UseGuards(JwtAuthGuard)
    book(@Body() param, @Request() req) {
        return this.bookingService.bookTickets({
            ticketIds: param.ticketIds,
            client: req.user.client,
        });
    }

    @Put('cancel/:bookingId')
    @UseGuards(JwtAuthGuard)
    cancel(@Param('bookingId') bookingId, @Request() req) {
        return this.bookingService.cancel(bookingId, req.user.client);
    }

    @Get('my-bookings')
    @UseGuards(JwtAuthGuard)
    myBookings(@Request() req) {
        return this.bookingService.myBookings(req.user.client);
    }

    @Get(':id')
    @UseGuards(JwtAuthGuard)
    findOne(@Param('id') id: string, @Request() req) {
        return this.bookingService.findOneBasedOnUser(id, req.user.client);
    }

    @Patch(':id')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(UserRole.ADMIN)
    update(@Param('id') id: string, @Body() updateBookingDto: UpdateBookingDto) {
        return this.bookingService.update(id, updateBookingDto);
    }

    @Delete(':id')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(UserRole.ADMIN)
    remove(@Param('id') id: string) {
        return this.bookingService.remove(id);
    }
}
