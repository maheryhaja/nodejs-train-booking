import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { HydratedDocument } from 'mongoose';
import { Client } from '../../client/entities/client.entity';
import { Ticket } from '../../ticket/entities/ticket.entity';

export enum BookingStatus {
    VALIDATED = 'VALIDATED',
    CANCELED = 'CANCELED',
    PAID = 'PAID',
}

@Schema()
export class Booking {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _id: any;

    @Prop([{ type: mongoose.Schema.Types.ObjectId, ref: 'Ticket', required: true }])
    tickets: Ticket[] | string[];

    @Prop({ type: String, enum: Object.values(BookingStatus), required: true })
    status: BookingStatus;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Client', required: true })
    client: Client;

    @Prop({ type: Date, required: true })
    createdAt: Date;

    @Prop({ type: Date })
    canceledAt?: Date;

    @Prop({ type: String, required: true })
    reference: String;
}

export type BookingDocument = HydratedDocument<Booking>;

export const BoookingSchema = SchemaFactory.createForClass(Booking);
