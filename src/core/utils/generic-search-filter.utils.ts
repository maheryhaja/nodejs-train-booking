import { Aggregate, Document, Model, PipelineStage } from 'mongoose';
import { PageCriteria } from '../types/page.interface';

export enum SortDirection {
    asc = 'asc',
    desc = 'desc',
}

export interface Sort {
    by: string;
    direction: SortDirection;
}

export type FilterFieldMap = Record<string, string>;

export type SearchFields = string[];

export type ListFilter = Record<string, Array<string | boolean | number | null>>;

export const buildSearchCriteria = (search: string | null, searchFields: Array<string>): Object =>
    search
        ? {
              $or: searchFields.map((field) => ({
                  [field]: { $regex: search, $options: 'i' },
              })),
          }
        : {};

export const buildFilterCriteria = (listBoxFilter: ListFilter, fieldMap: FilterFieldMap): object =>
    listBoxFilter
        ? Object.assign(
              {},
              ...Object.entries(listBoxFilter)
                  .map(([a, b]) => [a, Array.isArray(b) ? b : [b]])
                  .filter(([key, value]) => value && value.length && fieldMap[key as string])
                  .map(([key, value]) => ({
                      [fieldMap[key as string]]: {
                          $in: Array.isArray(value) ? value : [value],
                      },
                  }))
          )
        : {};

interface FilterParam<T extends Document> {
    model: Model<T>;
    populationStages?: PipelineStage[];
    listBoxFilterFieldMap: FilterFieldMap;
    searchFields: SearchFields;
    search: string;
    listBoxFilter: ListFilter;
    additionnalCriteria?: object;
    sort?: Sort;
    page: number;
    pageSize: number;
}

// eslint-disable-next-line max-lines-per-function
export const getFiltered = <T extends Document>({
    model,
    populationStages = [],
    listBoxFilterFieldMap,
    searchFields,
    search,
    listBoxFilter,
    additionnalCriteria = {},
    sort = undefined,
    page = 1,
    pageSize = 15,
}: FilterParam<T>): Aggregate<T[]> => {
    const searchCriteria = buildSearchCriteria(search, searchFields);
    const filterCriteria = buildFilterCriteria(listBoxFilter, listBoxFilterFieldMap);
    return model.aggregate([
        ...populationStages,
        {
            $match: {
                ...searchCriteria,
                ...filterCriteria,
                ...additionnalCriteria,
            },
        },
        ...(sort
            ? [
                  {
                      $sort: {
                          [sort.by]: (sort.direction === 'asc' ? 1 : -1) as 1 | -1,
                      },
                  },
              ]
            : []),
        {
            $skip: pageSize * (page - 1),
        },
        {
            $limit: pageSize,
        },
    ]);
};

export const buildPaginationAggregationStage = (pageCriteria: PageCriteria) => [
    {
        $skip: ((pageCriteria.page || 1) - 1) * (pageCriteria.pageSize || 15),
    },
    {
        $limit: pageCriteria.pageSize || 15,
    },
];
