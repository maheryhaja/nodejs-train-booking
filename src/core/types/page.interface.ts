export interface PageCriteria {
    page: number;
    pageSize: number;
}

export interface Paginated<T> {
    items: T[];
    totalItems: number;
}
