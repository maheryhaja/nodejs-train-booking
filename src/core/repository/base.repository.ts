import { Document, Model, PipelineStage, Query } from 'mongoose';
import { PageCriteria, Paginated } from '../types/page.interface';
import { buildPaginationAggregationStage } from '../utils/generic-search-filter.utils';

export interface GetPaginatedParam extends PageCriteria {
    populationStages: PipelineStage[];
    matchCriteria: object;
}

export abstract class BaseRepository<T extends Document, U> {
    readonly model: Model<T>;

    constructor(schemaModel: Model<T>) {
        this.model = schemaModel;
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    find(conditions: any): Query<T[], T> {
        return this.model.find(conditions);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    count(conditions: any): Promise<number> {
        return this.model.count(conditions).exec();
    }

    findById(id: string): Query<T | null, T> {
        return this.model.findById(id).lean();
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    findOne(conditions: any): Query<T | null, T> {
        return this.model.findOne(conditions, undefined).lean();
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    findAndUpdate(conditions: any, update: Partial<U>) {
        return this.model.findOneAndUpdate(conditions, update);
    }

    create(item: Omit<U, '_id'>): Promise<T> {
        return this.model.create(item);
    }

    delete(id: string): Promise<boolean> {
        return this.model.deleteOne({ _id: id }).then(() => true);
    }

    update(id: string, item: Partial<U>): Promise<T | null> {
        return this.model.findByIdAndUpdate(id, item, { new: true }).exec();
    }

    async getPaginated(param: GetPaginatedParam): Promise<Paginated<T>> {
        const aggregateStages = [
            ...param.populationStages,
            {
                $match: param.matchCriteria,
            },
        ];
        const items: T[] = await this.model.aggregate([...aggregateStages, ...buildPaginationAggregationStage(param)]);

        const { totalItems } = (await this.model.aggregate([...aggregateStages, { $count: 'totalItems' }]))[0] ?? {
            totalItems: 0,
        };

        return {
            items,
            totalItems,
        };
    }
}
