import { forwardRef, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import configuration from '../../config/configuration';
import { AppModule } from '../app.module';
import { ClientModule } from '../client/client.module';
import { UsersModule } from '../users/users.module';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';
import { JwtStrategy } from './jwt.strategy';
import { LocalAuthGuard } from './local-auth.guard';
import { LocalStrategy } from './local.strategy';

@Module({
    imports: [
        UsersModule,
        PassportModule,
        JwtModule.register({
            secret: configuration().jwt.secretKey,
            signOptions: { expiresIn: configuration().jwt.expiration },
        }),
        UsersModule,
        ClientModule,
        forwardRef(() => AppModule),
    ],
    providers: [AuthService, LocalStrategy, LocalAuthGuard, JwtStrategy, JwtAuthGuard],
    exports: [AuthService],
})
export class AuthModule {}
