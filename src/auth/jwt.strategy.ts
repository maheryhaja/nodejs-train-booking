import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ClientService } from '../client/client.service';
import { User, UserRole } from '../users/users.entity';
import { UsersService } from '../users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        readonly configService: ConfigService,
        private readonly userService: UsersService,
        private readonly clientService: ClientService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: configService.get<number>('jwt.secretKey'),
        });
    }

    async validate(payload: Pick<User, '_id' | 'username'>) {
        const user = await this.userService.findById(payload._id);
        if (user.role === UserRole.CLIENT) {
            user.client = await this.clientService.findClientByUserId(user._id);
            if (!user.client) {
                throw new HttpException('Client not found', HttpStatus.NOT_FOUND);
            }
        }
        return user;
    }
}
