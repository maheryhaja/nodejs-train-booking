import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { User } from '../users/users.entity';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
    constructor(private readonly userService: UsersService, private readonly jwtService: JwtService) {}

    async validateUser(username: string, pass: string): Promise<User | null> {
        const user = await this.userService.findOne(username);
        if (user && bcrypt.compare(pass, user.password)) {
            return user;
        }
        return null;
    }

    async login(user: User) {
        return {
            token: this.jwtService.sign({ username: user.username, _id: user._id }),
        };
    }
}
