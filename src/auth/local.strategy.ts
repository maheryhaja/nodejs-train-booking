import { HttpException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { ClientService } from '../client/client.service';
import { User, UserRole } from '../users/users.entity';
import { AuthService } from './auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService, private readonly clientService: ClientService) {
        super();
    }

    async validate(username: string, password: string): Promise<User> {
        const user = await this.authService.validateUser(username, password);

        if (!user) {
            throw new UnauthorizedException();
        }
        if (user.role === UserRole.CLIENT && !(await this.clientService.isClientUserActive(user._id))) {
            throw new HttpException('Account not activated', HttpStatus.UNAUTHORIZED);
        }

        return user;
    }
}
