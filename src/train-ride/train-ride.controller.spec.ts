import { Test, TestingModule } from '@nestjs/testing';
import { TrainRideController } from './train-ride.controller';
import { TrainRideService } from './train-ride.service';

describe('TrainRideController', () => {
  let controller: TrainRideController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TrainRideController],
      providers: [TrainRideService],
    }).compile();

    controller = module.get<TrainRideController>(TrainRideController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
