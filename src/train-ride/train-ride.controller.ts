import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { UserRole } from '../users/users.entity';
import { CreateTrainRideDto } from './dto/create-train-ride.dto';
import { UpdateTrainRideDto } from './dto/update-train-ride.dto';
import { TrainRideService } from './train-ride.service';

@Controller('train-ride')
export class TrainRideController {
    constructor(private readonly trainRideService: TrainRideService) {}

    @Post()
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(UserRole.ADMIN)
    create(@Body() createTrainRideDto: CreateTrainRideDto) {
        return this.trainRideService.create(createTrainRideDto);
    }

    @Get()
    getPaginated(@Query('page', ParseIntPipe) page, @Query('pageSize', ParseIntPipe) pageSize) {
        return this.trainRideService.getPaginated({ page, pageSize });
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.trainRideService.findOne(id);
    }

    @Patch(':id')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(UserRole.ADMIN)
    update(@Param('id') id: string, @Body() updateTrainRideDto: UpdateTrainRideDto) {
        return this.trainRideService.update(id, updateTrainRideDto);
    }

    @Delete(':id')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(UserRole.ADMIN)
    remove(@Param('id') id: string) {
        return this.trainRideService.remove(id);
    }
}
