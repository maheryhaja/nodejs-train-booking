import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '../auth/auth.module';
import { ClientModule } from '../client/client.module';
import { CounterModule } from '../counter/counter.module';
import { TrainRide, TrainRideSchema } from './entities/train-ride.entity';
import { TrainRideController } from './train-ride.controller';
import { TrainRideRepository } from './train-ride.repository';
import { TrainRideService } from './train-ride.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: TrainRide.name, schema: TrainRideSchema }]),
        CounterModule,
        AuthModule,
        forwardRef(() => ClientModule),
    ],
    controllers: [TrainRideController],
    providers: [TrainRideService, TrainRideRepository],
    exports: [TrainRideService],
})
export class TrainRideModule {}
