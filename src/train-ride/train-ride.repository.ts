import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { TrainRide, TrainRideDocument } from './entities/train-ride.entity';

@Injectable()
export class TrainRideRepository extends BaseRepository<TrainRideDocument, TrainRide> {
    constructor(@InjectModel(TrainRide.name) trainRideModel) {
        super(trainRideModel);
    }
}
