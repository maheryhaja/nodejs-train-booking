export const TRAIN_RIDE_POPULATION_STAGES = [
    {
        $lookup: {
            from: 'trains',
            localField: 'train',
            foreignField: '_id',
            as: 'train',
        },
    },
    {
        $unwind: {
            path: '$train',
            preserveNullAndEmptyArrays: true,
        },
    },
];
