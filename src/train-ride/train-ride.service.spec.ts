import { Test, TestingModule } from '@nestjs/testing';
import { TrainRideService } from './train-ride.service';

describe('TrainRideService', () => {
  let service: TrainRideService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TrainRideService],
    }).compile();

    service = module.get<TrainRideService>(TrainRideService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
