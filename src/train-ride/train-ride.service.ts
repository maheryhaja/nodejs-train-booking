import { Injectable } from '@nestjs/common';
import { PageCriteria } from '../core/types/page.interface';
import { CounterService } from '../counter/counter.service';
import { CreateTrainRideDto } from './dto/create-train-ride.dto';
import { UpdateTrainRideDto } from './dto/update-train-ride.dto';
import { TRAIN_RIDE_POPULATION_STAGES } from './train-ride.constant';
import { TrainRideRepository } from './train-ride.repository';

@Injectable()
export class TrainRideService {
    constructor(private readonly trainRideRepository: TrainRideRepository, private readonly counterService: CounterService) {}

    async create(createTrainRideDto: CreateTrainRideDto) {
        return this.trainRideRepository.create({
            ...createTrainRideDto,
            reference: await this.counterService.requestReference('TR'),
        });
    }

    getPaginated(criteria: PageCriteria) {
        const matchCriteria = {
            departure: { $gt: new Date() },
        };

        return this.trainRideRepository.getPaginated({
            matchCriteria,
            ...criteria,
            populationStages: TRAIN_RIDE_POPULATION_STAGES,
        });
    }

    findAll() {
        return this.trainRideRepository.find({});
    }

    findOne(id: string) {
        return this.trainRideRepository.findOne({ _id: id });
    }

    update(id: string, updateTrainRideDto: UpdateTrainRideDto) {
        return this.trainRideRepository.update(id, updateTrainRideDto);
    }

    remove(id: string) {
        return this.trainRideRepository.delete(id);
    }
}
