import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Train } from '../../train/entities/train.entity';

@Schema()
export class TrainRide {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _id: any;

    @Prop({ type: Date, required: true })
    departure: Date;

    @Prop({ type: Date, required: true })
    arrival: Date;

    @Prop({ type: String, required: true })
    reference: string;

    @Prop({ type: String, required: true })
    from: string;

    @Prop({ type: String, required: true })
    to: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Train', required: true })
    train: Train;
}

export type TrainRideDocument = mongoose.HydratedDocument<TrainRide>;

export const TrainRideSchema = SchemaFactory.createForClass(TrainRide);
TrainRideSchema.pre('find', function (next) {
    this.populate('train');
    next();
});

TrainRideSchema.pre('findOne', function (next) {
    this.populate('train');
    next();
});
