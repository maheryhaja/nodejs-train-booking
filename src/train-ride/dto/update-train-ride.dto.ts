import { TrainRide } from '../entities/train-ride.entity';

export type UpdateTrainRideDto = Partial<TrainRide>;
