import { TrainRide } from '../entities/train-ride.entity';

export type CreateTrainRideDto = Omit<TrainRide, '_id'>;
