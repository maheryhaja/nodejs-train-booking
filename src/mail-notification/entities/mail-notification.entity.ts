import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

@Schema()
export class NotificationAttachment {
    @Prop({ type: String, required: true })
    filename: string;

    @Prop({ type: String, required: true })
    path: string;
}

const NotificationAttachmentSchema = SchemaFactory.createForClass(NotificationAttachment);

export enum NotificationStatus {
    PENDING = 'PENDING',
    SENDING = 'SENDING',
    SENT = 'SENT',
    ERROR = 'ERROR',
}

@Schema()
export class MailNotification {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _id: any;

    @Prop({ type: Date, require: true })
    createdAt: Date;

    @Prop({ type: Date })
    updatedAt?: Date;

    @Prop({ type: String, enum: Object.values(NotificationStatus), required: true })
    status: NotificationStatus;

    @Prop({ type: String, required: true })
    to: string;

    @Prop({ type: String, required: true })
    content: string;

    @Prop({ type: String, required: true })
    subject: string;

    @Prop({ type: String })
    errorMessage?: string;

    @Prop({ type: [] })
    attachments?: NotificationAttachment[];
}

export const MailNotificationSchema = SchemaFactory.createForClass(MailNotification);
export type MailNotificationDocument = HydratedDocument<MailNotification>;
