import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppModule } from '../app.module';
import { MailModule } from '../mail/mail.module';
import { MailNotification, MailNotificationSchema } from './entities/mail-notification.entity';
import { MailNotificationRepository } from './mail-notification.repository';
import { MailNotificationService } from './mail-notification.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: MailNotification.name, schema: MailNotificationSchema }]),
        MailModule,
        forwardRef(() => AppModule),
    ],
    providers: [MailNotificationService, MailNotificationRepository],
    exports: [MailNotificationService],
})
export class MailNotificationModule {}
