import { Injectable } from '@nestjs/common';
import { MailService } from '../mail/mail.service';
import { CreateMailNotificationDto } from './dto/create-mail-notification.dto';
import { MailNotification, NotificationStatus } from './entities/mail-notification.entity';
import { MailNotificationRepository } from './mail-notification.repository';

@Injectable()
export class MailNotificationService {
    constructor(
        private readonly mailNotificationRepository: MailNotificationRepository,
        private readonly mailService: MailService
    ) {}

    async create(createMailNotificationDto: CreateMailNotificationDto) {
        await this.mailNotificationRepository.create({
            ...createMailNotificationDto,
            createdAt: new Date(),
            status: NotificationStatus.PENDING,
        });
    }

    async sendAllPendingMail() {
        const notifications: MailNotification[] = await this.mailNotificationRepository
            .find({ status: NotificationStatus.PENDING })
            .exec();
        for (const notification of notifications) {
            try {
                await this.mailService.sendMail({
                    from: 'Train Booking Corp',
                    content: notification.content,
                    subject: notification.subject,
                    to: notification.to,
                });

                await this.mailNotificationRepository.update(notification._id, {
                    status: NotificationStatus.SENT,
                    updatedAt: new Date(),
                });
            } catch (error) {
                this.mailNotificationRepository.update(notification._id, {
                    status: NotificationStatus.ERROR,
                    errorMessage: 'error while sending email',
                    updatedAt: new Date(),
                });
            }
        }
    }
}
