import { MailNotification } from '../entities/mail-notification.entity';

export type CreateMailNotificationDto = Pick<MailNotification, 'to' | 'content' | 'subject'>;
