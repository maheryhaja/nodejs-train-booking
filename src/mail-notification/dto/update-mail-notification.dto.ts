import { CreateMailNotificationDto } from './create-mail-notification.dto';

export type UpdateMailNotificationDto = Partial<CreateMailNotificationDto>;
