import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { MailNotification, MailNotificationDocument } from './entities/mail-notification.entity';

@Injectable()
export class MailNotificationRepository extends BaseRepository<MailNotificationDocument, MailNotification> {
    constructor(@InjectModel(MailNotification.name) mailNotificationModel) {
        super(mailNotificationModel);
    }
}
