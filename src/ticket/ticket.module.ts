import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '../auth/auth.module';
import { CounterModule } from '../counter/counter.module';
import { TrainRideModule } from '../train-ride/train-ride.module';
import { TrainModule } from '../train/train.module';
import { Ticket, TicketSchema } from './entities/ticket.entity';
import { TicketController } from './ticket.controller';
import { TicketRepository } from './ticket.repository';
import { TicketService } from './ticket.service';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Ticket.name, schema: TicketSchema }]),
        CounterModule,
        TrainModule,
        TrainRideModule,
        AuthModule,
    ],
    controllers: [TicketController],
    providers: [TicketService, TicketRepository],
    exports: [TicketService],
})
export class TicketModule {}
