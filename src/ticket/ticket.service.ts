import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Client } from '../client/entities/client.entity';
import { Paginated } from '../core/types/page.interface';
import { CounterService } from '../counter/counter.service';
import { TrainService } from '../train/train.service';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { GetAvailableTicketCriteria } from './dto/get-available-ticket.criteria';
import { UpdateTicketDto } from './dto/update-ticket.dto';
import { Ticket, TicketStatus } from './entities/ticket.entity';
import { TICKET_POPULATION_STAGES } from './ticket.constant';
import { TicketRepository } from './ticket.repository';

@Injectable()
export class TicketService {
    constructor(
        private readonly ticketRepository: TicketRepository,
        private readonly counterService: CounterService,
        private readonly trainService: TrainService
    ) {}

    async create(createTicketDto: CreateTicketDto) {
        return this.ticketRepository.create({
            ...createTicketDto,
            reference: await this.counterService.requestReference('TK'),
        });
    }

    findAll() {
        return this.ticketRepository.find({});
    }

    findOne(id: string) {
        return this.ticketRepository.findById(id);
    }

    update(id: string, updateTicketDto: UpdateTicketDto) {
        return this.ticketRepository.update(id, updateTicketDto);
    }

    remove(id: string) {
        return this.ticketRepository.delete(id);
    }

    async getAvailableTickets(criteria: GetAvailableTicketCriteria): Promise<Paginated<Ticket>> {
        if (!criteria.trainRideId) {
            throw new HttpException('trainRideId required', HttpStatus.FAILED_DEPENDENCY);
        }
        const filterCriteria = {
            ...(criteria.seatType ? { 'seat.type': criteria.seatType } : {}),
            'trainRide._id': new mongoose.Types.ObjectId(criteria.trainRideId),
            'status': TicketStatus.AVAILABLE,
        };

        return this.ticketRepository.getPaginated({
            matchCriteria: filterCriteria,
            page: criteria.page,
            pageSize: criteria.pageSize,
            populationStages: TICKET_POPULATION_STAGES,
        });
    }

    async isAllTicketsAvailable(ticketIds: string[]): Promise<boolean> {
        const availableTickets: number = await this.ticketRepository.count({
            status: TicketStatus.AVAILABLE,
            _id: { $in: ticketIds },
        });
        return ticketIds.length === availableTickets;
    }

    async getTickets(ticketIds: string[]): Promise<Ticket[]> {
        return this.ticketRepository
            .find({
                _id: { $in: ticketIds },
            })
            .populate('seat trainRide')
            .lean()
            .exec();
    }

    async markAsBooked(ticketIds: string[], bookedBy: Client) {
        await this.ticketRepository
            .findAndUpdate(
                { _id: { $in: ticketIds } },
                {
                    status: TicketStatus.BOOKED,
                    bookedBy,
                }
            )
            .lean()
            .exec();
    }

    async cancel(ticketIds: string[]) {
        await this.ticketRepository
            .findAndUpdate(
                { _id: { $in: ticketIds } },
                {
                    status: TicketStatus.AVAILABLE,
                    bookedBy: null,
                }
            )
            .lean()
            .exec();
    }
}
