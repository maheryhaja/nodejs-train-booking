import { PageCriteria } from '../../core/types/page.interface';
import { TrainSeatType } from '../../train-seat/entities/train-seat.entity';

export interface GetAvailableTicketCriteria extends PageCriteria {
    trainRideId: string;
    seatType?: TrainSeatType;
}
