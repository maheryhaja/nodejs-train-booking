import { Ticket } from '../entities/ticket.entity';

export type UpdateTicketDto = Partial<Ticket>;
