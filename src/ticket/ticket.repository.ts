import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { Ticket, TicketDocument } from './entities/ticket.entity';

export class TicketRepository extends BaseRepository<TicketDocument, Ticket> {
    constructor(@InjectModel(Ticket.name) ticketModel) {
        super(ticketModel);
    }
}
