import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Client } from '../../client/entities/client.entity';
import { TrainRide } from '../../train-ride/entities/train-ride.entity';
import { TrainSeat } from '../../train-seat/entities/train-seat.entity';

export enum TicketStatus {
    BOOKED = 'BOOKED',
    AVAILABLE = 'AVAILABLE',
}

@Schema()
export class Ticket {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _id: any;

    @Prop({ type: String, required: true })
    reference: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'TrainRide', required: true })
    trainRide: TrainRide;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'TrainSeat', required: true })
    seat: TrainSeat;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Client', required: false })
    bookedBy?: Client;

    @Prop({ type: Number, required: true })
    price: number;

    @Prop({ type: String, enum: Object.values(TicketStatus), required: true })
    status: TicketStatus;
}

export type TicketDocument = mongoose.HydratedDocument<Ticket>;

export const TicketSchema = SchemaFactory.createForClass(Ticket);
