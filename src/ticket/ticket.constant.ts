export const TICKET_POPULATION_STAGES = [
    {
        $lookup: {
            from: 'trainseats',
            localField: 'seat',
            foreignField: '_id',
            as: 'seat',
        },
    },
    {
        $unwind: {
            path: '$seat',
            preserveNullAndEmptyArrays: true,
        },
    },
    {
        $lookup: {
            from: 'trainrides',
            localField: 'trainRide',
            foreignField: '_id',
            as: 'trainRide',
        },
    },
    {
        $unwind: {
            path: '$trainRide',
            preserveNullAndEmptyArrays: true,
        },
    },
];
