import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { Counter, CounterDocument } from './counter.entity';

@Injectable()
export class CounterRepository extends BaseRepository<CounterDocument, Counter> {
    constructor(@InjectModel(Counter.name) counterModel) {
        super(counterModel);
    }
}
