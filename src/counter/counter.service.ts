import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Counter } from './counter.entity';
import { CounterRepository } from './counter.repository';
import { CounterUtils } from './counter.utils';

@Injectable()
export class CounterService {
    constructor(private readonly counterRepository: CounterRepository) {}

    private async getCounter(counterKey: string): Promise<Counter> {
        const foundCounter = await this.counterRepository.findOne({ counterKey }).exec();
        if (!foundCounter) {
            return this.counterRepository.create({
                count: 0,
                counterKey,
            });
        }
        return foundCounter;
    }

    async requestReference(counterKey: string): Promise<string> {
        const counter = await this.getCounter(counterKey);
        if (!counter) {
            throw new HttpException('Counter collection not found', HttpStatus.NOT_FOUND);
        }
        const reference = CounterUtils.getReference(counterKey, counter.count);
        counter.count++;
        await this.counterRepository.update(counter._id, counter);
        return reference;
    }
}
