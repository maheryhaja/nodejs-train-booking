import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Counter, CounterSchema } from './counter.entity';
import { CounterRepository } from './counter.repository';
import { CounterService } from './counter.service';

@Module({
    imports: [MongooseModule.forFeature([{ name: Counter.name, schema: CounterSchema }])],
    providers: [CounterService, CounterRepository],
    exports: [CounterService],
})
export class CounterModule {}
