const LIMIT_CLIENT_ID = 9999;

export namespace CounterUtils {
    export const getReference = (counterKey: string, counterParam: number): string => {
        if (counterParam > LIMIT_CLIENT_ID) {
            throw new Error('Max client id reached');
        }

        const paddedCounter = `${counterParam}`.padStart(4, '0');
        return `${counterKey}_${paddedCounter}`;
    };
}
