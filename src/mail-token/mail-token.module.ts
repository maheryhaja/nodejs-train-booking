import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MailToken, MailTokenSchema } from './entities/mail-token.entity';
import { MailTokenRepository } from './mail-token.repository';
import { MailTokenService } from './mail-token.service';

@Module({
    imports: [MongooseModule.forFeature([{ name: MailToken.name, schema: MailTokenSchema }])],
    providers: [MailTokenService, MailTokenRepository],
    exports: [MailTokenService],
})
export class MailTokenModule {}
