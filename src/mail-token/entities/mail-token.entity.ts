import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export enum MailTokenType {
    CLIENT_ACTIVATION = 'CLIENT_ACTIVATION',
}

export enum MailTokenStatus {
    CREATED = 'CREATED',
    USED = 'USED',
    EXPIRED = 'EXPIRED',
}

@Schema()
export class MailToken {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _id: any;

    @Prop({ type: String, enum: Object.values(MailTokenType), required: true })
    type: MailTokenType;

    @Prop({ type: Date, required: true })
    createdAt: Date;

    @Prop({ type: Date })
    updatedAt?: Date;

    @Prop({ type: String, required: true })
    entityId: string;

    @Prop({ type: Date })
    expiredAt?: Date;

    @Prop({ type: String, required: true })
    token: string;

    @Prop({ type: String, enum: Object.values(MailTokenStatus), required: true })
    status: MailTokenStatus;
}

export type MailTokenDocument = HydratedDocument<MailToken>;

export const MailTokenSchema = SchemaFactory.createForClass(MailToken);
