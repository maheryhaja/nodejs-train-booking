import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { MailToken, MailTokenDocument } from './entities/mail-token.entity';

@Injectable()
export class MailTokenRepository extends BaseRepository<MailTokenDocument, MailToken> {
    constructor(@InjectModel(MailToken.name) mailTokenModel) {
        super(mailTokenModel);
    }
}
