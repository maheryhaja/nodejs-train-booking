import { MailToken } from '../entities/mail-token.entity';

export type CreateMailTokenDto = Omit<MailToken, '_id'>;
