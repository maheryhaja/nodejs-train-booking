import { MailToken } from '../entities/mail-token.entity';

export type UpdateMailTokenDto = Partial<MailToken>;
