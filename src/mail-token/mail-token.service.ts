import { Injectable } from '@nestjs/common';
import { randomUUID } from 'crypto';
import { CreateMailTokenDto } from './dto/create-mail-token.dto';
import { UpdateMailTokenDto } from './dto/update-mail-token.dto';
import { MailToken, MailTokenStatus, MailTokenType } from './entities/mail-token.entity';
import { MailTokenRepository } from './mail-token.repository';

@Injectable()
export class MailTokenService {
    constructor(private readonly mailTokenRepository: MailTokenRepository) {}

    create(createMailTokenDto: CreateMailTokenDto) {
        return this.mailTokenRepository.create(createMailTokenDto);
    }

    findAll() {
        return this.findAll();
    }

    findOne(id: string) {
        return this.mailTokenRepository.findById(id);
    }

    findByToken(token: string): Promise<MailToken | undefined> {
        return this.mailTokenRepository.findOne({ token });
    }

    private async isTokenUsed(token: string): Promise<boolean> {
        return (await this.mailTokenRepository.count({ token })) > 0;
    }

    private async generateToken(): Promise<string> {
        const token = randomUUID().slice(9);
        if (!(await this.isTokenUsed(token))) {
            return token;
        }
        return this.generateToken();
    }

    async requestToken(type: MailTokenType, entityId: string): Promise<MailToken> {
        return this.create({
            createdAt: new Date(),
            entityId,
            status: MailTokenStatus.CREATED,
            token: await this.generateToken(),
            type,
        });
    }

    async markAsUserd(token: string) {
        await this.mailTokenRepository.findAndUpdate({ token }, {});
    }

    update(id: string, updateMailTokenDto: UpdateMailTokenDto) {
        return this.mailTokenRepository.update(id, updateMailTokenDto);
    }

    remove(id: string) {
        return this.mailTokenRepository.delete(id);
    }
}
