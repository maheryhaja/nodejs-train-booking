import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { MongooseModule } from '@nestjs/mongoose';
import configuration from '../config/configuration';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { RolesGuard } from './auth/roles.guard';
import { BookingModule } from './booking/booking.module';
import { ClientModule } from './client/client.module';
import { CounterModule } from './counter/counter.module';
import { DbScriptModule } from './db-script/db-script.module';
import { MailNotificationModule } from './mail-notification/mail-notification.module';
import { MailTokenModule } from './mail-token/mail-token.module';
import { MailModule } from './mail/mail.module';
import { TicketModule } from './ticket/ticket.module';
import { TrainRideModule } from './train-ride/train-ride.module';
import { TrainSeatModule } from './train-seat/train-seat.module';
import { UsersModule } from './users/users.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            load: [configuration],
        }),
        AuthModule,
        UsersModule,
        DbScriptModule,
        MongooseModule.forRoot(configuration().mongodb.dbURI),
        MailModule,
        MailNotificationModule,
        ClientModule,
        CounterModule,
        BookingModule,
        TrainSeatModule,
        TrainRideModule,
        TicketModule,
        MailTokenModule,
    ],
    controllers: [AppController],
    providers: [
        AppService,
        ConfigService,
        {
            provide: APP_GUARD,
            useClass: RolesGuard,
        },
    ],
    exports: [ConfigService],
})
export class AppModule {}
