import { Train } from '../entities/train.entity';

export type CreateTrainDto = Omit<Train, '_id'>;
