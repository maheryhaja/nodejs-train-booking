import { Train } from '../entities/train.entity';

export type UpdateTrainDto = Partial<Train>;
