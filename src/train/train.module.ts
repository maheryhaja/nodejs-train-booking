import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from '../auth/auth.module';
import { ClientModule } from '../client/client.module';
import { Train, TrainSchema } from './entities/train.entity';
import { TrainController } from './train.controller';
import { TrainRepository } from './train.repository';
import { TrainService } from './train.service';

@Module({
    imports: [MongooseModule.forFeature([{ name: Train.name, schema: TrainSchema }]), ClientModule, AuthModule],
    controllers: [TrainController],
    providers: [TrainService, TrainRepository],
    exports: [TrainService],
})
export class TrainModule {}
