import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

@Schema()
export class Train {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    _id: any;

    @Prop({ type: String, required: true })
    name: string;

    @Prop({ type: String, required: true })
    trainModel: string;
}

export type TrainDocument = HydratedDocument<Train>;

export const TrainSchema = SchemaFactory.createForClass(Train);
