import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '../core/repository/base.repository';
import { Train, TrainDocument } from './entities/train.entity';

@Injectable()
export class TrainRepository extends BaseRepository<TrainDocument, Train> {
    constructor(@InjectModel(Train.name) trainModel) {
        super(trainModel);
    }
}
