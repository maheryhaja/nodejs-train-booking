import { Injectable } from '@nestjs/common';
import { CreateTrainDto } from './dto/create-train.dto';
import { UpdateTrainDto } from './dto/update-train.dto';
import { TrainRepository } from './train.repository';

@Injectable()
export class TrainService {
    constructor(private readonly trainRepository: TrainRepository) {}

    create(createTrainDto: CreateTrainDto) {
        return this.trainRepository.create(createTrainDto);
    }

    findAll() {
        return this.trainRepository.find({});
    }

    findOne(id: string) {
        return this.trainRepository.findOne({ _id: id });
    }

    update(id: string, updateTrainDto: UpdateTrainDto) {
        return this.trainRepository.update(id, updateTrainDto);
    }

    remove(id: string) {
        return this.trainRepository.delete(id);
    }
}
